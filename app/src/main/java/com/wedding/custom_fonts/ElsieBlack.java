package com.wedding.custom_fonts;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class ElsieBlack extends TextView {
	public ElsieBlack(Context context) {
		super(context);
		setFont();

	}
public ElsieBlack(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFont();
	}
	public ElsieBlack(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setFont();
	}

	private void setFont() {
		Typeface font = Typeface.createFromAsset(getContext().getAssets(),"fonts/Elsie-Black.otf");
		setTypeface(font);
	}


}