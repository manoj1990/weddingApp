package com.wedding.custom_fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user on 7/13/2016.
 */

public class SFRegular extends TextView {
    public SFRegular(Context context) {
        super(context);
        setFont();
    }
    public SFRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public SFRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),"fonts/SF-UI-Text-Regular_0.otf");
        setTypeface(font);
    }
}
