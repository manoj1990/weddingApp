package com.wedding.adapters.VendorAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedding.MainActivity;
import com.wedding.R;
import com.wedding.bean.VendorBean.VendorsBean;
import com.wedding.fragment.before_login_fragments.VendorsFrag.VendorDetailFrag;

import java.util.ArrayList;

/**
 * Created by user on 7/19/2016.
 */

public class VendorsAdapter extends RecyclerView.Adapter<VendorsAdapter.ViewHolder> {
    private LayoutInflater _layoutInflator;
    private Context context;
    private ArrayList<VendorsBean> _listItem = new ArrayList<>();


    public VendorsAdapter(ArrayList<VendorsBean> projectsItems, Context context) {
        _listItem = projectsItems;
        this.context = context;
        this._layoutInflator = LayoutInflater.from(context);
    }

    @Override
    public VendorsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_pager_item, viewGroup, false);

        VendorsAdapter.ViewHolder viewholder = new VendorsAdapter.ViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(VendorsAdapter.ViewHolder holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity activity = (MainActivity)context;
                activity.replaceFragmentwithStack(new VendorDetailFrag(),"vendor_detail");

            }
        });

    }

    @Override
    public int getItemCount() {
        return _listItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(final View itemView) {
            super(itemView);
        }

    }
}
