package com.wedding.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedding.MainActivity;
import com.wedding.R;
import com.wedding.bean.RealWeddingBean.HomeWeddingBean;
import com.wedding.fragment.before_login_fragments.VendorsFrag.VendorFrag;

import java.util.ArrayList;

/**
 * Created by user on 7/20/2016.
 */

public class DummyAdapter extends RecyclerView.Adapter<DummyAdapter.ViewHolder> {
    private LayoutInflater _layoutInflator;
    private Context context;
    private ArrayList<HomeWeddingBean> _listItem = new ArrayList<>();


    public DummyAdapter(ArrayList<HomeWeddingBean> projectsItems, Context context) {
        _listItem = projectsItems;
        this.context = context;
        this._layoutInflator = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.real_wedding_item, viewGroup, false);

        ViewHolder viewholder = new ViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(DummyAdapter.ViewHolder holder, int position) {

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity activity = (MainActivity)context;
                activity.replaceFragmentwithStack(new VendorFrag("success"),"vendor_frag");

            }
        });
    }

    @Override
    public int getItemCount() {
        return _listItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(final View itemView) {
            super(itemView);
        }


    }
}
