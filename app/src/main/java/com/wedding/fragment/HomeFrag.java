package com.wedding.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedding.R;
import com.wedding.adapters.DummyAdapter;
import com.wedding.bean.RealWeddingBean.HomeWeddingBean;
import com.wedding.fragment.before_login_fragments.VendorsFrag.VendorFrag;

import java.util.ArrayList;

/**
 * Created by user on 7/13/2016.
 */

public class HomeFrag extends Fragment {

    View v;

    RecyclerView real_wedding_rv,blogs_rv;
    ArrayList<HomeWeddingBean> home_wed_al = new ArrayList<>();
    DummyAdapter dummyAdapter;
    LinearLayoutManager horizontal_llm,horizontal_llm1;

    public HomeFrag(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(v == null) {
            v = inflater.inflate(R.layout.content_main, container, false);
            init(v);
        }

        return v;
    }

    private void init(View v) {

            real_wedding_rv = (RecyclerView)v.findViewById(R.id.real_wedding_rv);
            blogs_rv = (RecyclerView)v.findViewById(R.id.blogs_rv);

            horizontal_llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            real_wedding_rv.setLayoutManager(horizontal_llm);

            horizontal_llm1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            blogs_rv.setLayoutManager(horizontal_llm1);

            loadData();
            dummyAdapter = new DummyAdapter(home_wed_al,getActivity());
            real_wedding_rv.setAdapter(dummyAdapter);
            blogs_rv.setAdapter(dummyAdapter);



    }

    private void loadData(){
        for(int i = 0; i < 5; i++){
            HomeWeddingBean bean = new HomeWeddingBean();
            home_wed_al.add(bean);

        }
    }

    }