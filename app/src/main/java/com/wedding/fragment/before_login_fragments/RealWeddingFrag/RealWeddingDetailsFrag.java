package com.wedding.fragment.before_login_fragments.RealWeddingFrag;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedding.R;

/**
 * Created by user on 7/19/2016.
 */

public class RealWeddingDetailsFrag extends Fragment {

    public RealWeddingDetailsFrag()
    {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.vendor_detail_frag,container,false);

        return v;
    }
}
