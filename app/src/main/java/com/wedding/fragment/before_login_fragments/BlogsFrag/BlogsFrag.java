package com.wedding.fragment.before_login_fragments.BlogsFrag;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedding.R;

/**
 * Created by user on 7/19/2016.
 */

public class BlogsFrag extends Fragment {

    public BlogsFrag()
    {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.blogs_frag,container,false);

        return v;
    }
}
