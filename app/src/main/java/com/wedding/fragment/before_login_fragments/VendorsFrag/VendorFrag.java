package com.wedding.fragment.before_login_fragments.VendorsFrag;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wedding.R;
import com.wedding.adapters.VendorAdapter.VendorsAdapter;
import com.wedding.bean.VendorBean.VendorsBean;

import java.util.ArrayList;

/**
 * Created by user on 7/19/2016.
 */

public class VendorFrag extends Fragment {

    String Msg;
    View v;
    RecyclerView vendor_frag_rv;
    ArrayList<VendorsBean> vendor_al = new ArrayList<>();
    VendorsAdapter vendorsAdapter;

    public VendorFrag (String id) {
        this.Msg = id;

    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(v == null) {
            v = inflater.inflate(R.layout.vendors_frag, container, false);
            init(v);
        }

        Log.e("MSG",Msg);

        return v;
    }

    private void init(View v) {

        vendor_frag_rv = (RecyclerView)v.findViewById(R.id.vendor_frag_rv);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL,false);
        vendor_frag_rv.setLayoutManager(llm);

        loadData();
        vendorsAdapter = new VendorsAdapter(vendor_al,getActivity());
        vendor_frag_rv.setAdapter(vendorsAdapter);

    }

    private void loadData() {
        for (int i = 0; i < 5; i++) {
            VendorsBean bean = new VendorsBean();
            vendor_al.add(bean);

        }

    }
}
