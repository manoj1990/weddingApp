package com.wedding.fragment.before_login_fragments.ShopFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedding.R;

/**
 * Created by user on 7/19/2016.
 */

public class ShopsFrag extends Fragment {

    public ShopsFrag()
    {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.shops_frag,container,false);

        return v;
    }
}
