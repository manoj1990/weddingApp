package com.wedding.fragment.before_login_fragments.VendorsFrag;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedding.R;

/**
 * Created by user on 7/19/2016.
 */

public class VendorPortfolioFrag extends Fragment{

    public VendorPortfolioFrag ()
    {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.vendor_type_portfolio,container,false);

        return v;
    }
}
